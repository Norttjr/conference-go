import email
from django.http import JsonResponse

from .models import AccountVO, Attendee
from .models import ConferenceVO
import json
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttenddeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
        "has_account",
    ]
    encoders = {
        "conference": ConferenceVODetailEncoder(),
    }

    def get_extra_data(self, o):
        # Get the count of AccountVO objects with email equal to o.email
        count = AccountVO.objects.filter(email=o.email).count()

        if count > 0:
            return {"has_account":True}
        # Return a dictionary with "has_account": True if count > 0
        # Otherwise, return a dictionary with "has_account": False
        else:
            return {"has_account"}: False


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):

    # Lists the attendees names and the link to the attendee
    # for the specified conference id.

    # Returns a dictionary with a single key "attendees" which
    # is a list of attendee names and URLS. Each entry in the list
    # is a dictionary that contains the name of the attendee and
    # the link to the attendee's information.
    if request.method == "GET":
        attendees = {
            "attendees": [
                {
                    "name": a.name,
                    "href": a.get_api_url(),
                }
                for a in Attendee.objects.filter(conference=conference_vo_id)
            ]
        }

        return JsonResponse({"attendees": attendees})
    else:
        content = json.loads(request.body)

        # Get the Conference object and put it in the content dict
        try:
            conference_href = content["conference"]
            conference = ConferenceVO.objects.get(import_href=conference_href)
            content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
        # this code is for when i refactor everything to use the encoders
    # attendees = Attendee.objects.filter(conference=conference_id)
    # return JsonResponse(
    #     {"attendees": attendees},
    #     encoder=AttendeeListEncoder,
    # )


def api_show_attendee(request, pk):

    # Returns the details for the Attendee model specified
    # by the pk parameter.

    # This should return a dictionary with email, name,
    # company name, created, and conference properties for
    # the specified Attendee instance.
    attendee = Attendee.objects.get(id=pk)
    return JsonResponse(
        {
            "email": attendee.email,
            "name": attendee.name,
            "company_name": attendee.company_name,
            "created": attendee.created,
            "conference": {
                "name": attendee.conference.name,
                "href": attendee.conference.get_api_url(),
            },
        }
    )
