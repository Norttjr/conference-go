from django.http import JsonResponse
from .models import Presentation
from common.json import ModelEncoder
import json
from events.models import Conference
from django.views.decorators.http import require_http_methods
from events.api_views import ConferenceListEncoder

# from presentations.api_views import presentation_message
import pika


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceListEncoder(),
    }

    def get_extra_data(self, o):
        return {"status": o.status.name}


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = ["name"]


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    # """
    # Lists the presentation titles and the link to the
    # presentation for the specified conference id.

    # Returns a dictionary with a single key "presentations"
    # which is a list of presentation titles and URLS. Each
    # entry in the list is a dictionary that contains the
    # title of the presentation, the name of its status, and
    # the link to the presentation's information.

    # {
    #     "presentations": [
    #         {
    #             "title": presentation's title,
    #             "status": presentation's status name
    #             "href": URL to the presentation,
    #         },
    #         ...
    #     ]
    # }
    # """
    # if request.method == "GET":
    #     presentations = [
    #         {
    #             "title": p.title,
    #             "status": p.status.name,
    #             "href": p.get_api_url(),
    #         }
    #         for p in Presentation.objects.filter(conference=conference_id)
    #     ]
    if request.method == "GET":
        presentations = Presentation.objects.all()
        return JsonResponse(
            {"presentations": presentations},
            encoder=PresentationListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference

        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Conference id"},
                status=400,
            )
        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )


def api_show_presentation(request, pk):
    presentation = Presentation.objects.get(id=pk)
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )


def presentation_message(message, queue_name):
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue=queue_name)
    channel.basic_publish(
        exchange="",
        routing_key=queue_name,
        body=message,
    )
    connection.close()


@require_http_methods(["PUT"])
def api_approve_presentation(request, pk):
    presentation = Presentation.objects.get(id=pk)
    presentation.approve()
    message = {
        "presenter_name": presentation.presenter_name,
        "presenter_email": presentation.presenter_email,
        "title": presentation.title,
    }
    presentation_message(json.dumps(message), "presentation_approvals")
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def api_reject_presentation(request, pk):
    presentation = Presentation.objects.get(id=pk)
    presentation.reject()
    message = {
        "presenter_name": presentation.presenter_name,
        "presenter_email": presentation.presenter_email,
        "title": presentation.title,
    }
    presentation_message(json.dumps(message), "presentation_rejections")
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )
